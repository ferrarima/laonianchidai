#include "platform.h"
#include "logging.h"
#include "utils.h"
#include "myassert.h"

static LogLevel gLevel = LogLevel::DEBUG;

void setLogLevel(LogLevel lv)
{
    gLevel = lv;
}

void logError(const char *p)
{
    if (gLevel<LogLevel::ERROR) return;
    Serial.print("[ERROR]   ");
    Serial.println(p);
}

void logWarning(const char *p)
{
    if (gLevel<LogLevel::WARNING) return;
    Serial.print("[WARNING] ");
    Serial.println(p);
}

void logInfo(const char *p)
{
    if (gLevel<LogLevel::INFO) return;
    Serial.print("[INFO]    ");
    Serial.println(p);
}

void logDebug(const char *p)
{
    if (gLevel<LogLevel::DEBUG) return;
    Serial.print("[DEBUG]   ");
    Serial.println(p);
}

void logError(int x) {
	if (gLevel<LogLevel::ERROR) return;
	Serial.print("[ERROR]   ");
	Serial.println(x);
}
void logWarning(int x) {
	if (gLevel<LogLevel::WARNING) return;
	Serial.print("[WARNING] ");
	Serial.println(x);
}
void logInfo(int x) {
	if (gLevel<LogLevel::INFO) return;
	Serial.print("[INFO]    ");
	Serial.println(x);
}
void logDebug(int x) {
	if (gLevel<LogLevel::DEBUG) return;
	Serial.print("[DEBUG]   ");
	Serial.println(x);
}
