#include "colors.h"
#include "platform.h"
#include "myassert.h"


bool ColorSet::empty()
{
	for (int i = 0; i < NUM_COLOR; i++)
		if (v[i]) return false;
	return true;
}

bool ColorSet::randomPick(Color & ret)
{
	if (empty()) return false;
	Color tmp[NUM_COLOR];
	int tmpn = 0;
	for (int i = 0; i < NUM_COLOR; ++i) {
		if (v[i]) tmp[tmpn++] = (Color)(i);
	}
	int idx = random(0, tmpn - 1);
	ret = tmp[idx];
	return true;
}

bool ColorSet::erase(Color c)
{
	bool ret = v[(int)c];
	v[(int)c] = false;
	return ret;
}

int ColorSet::size()
{
	int ret = 0;
	for (int i = 0; i < NUM_COLOR; ++i) {
		if (v[i]) ++ret;
	}
	return ret;
}

void ColorSet::insert(Color color)
{
	v[(int)color] = true;
}

bool ColorSet::has(Color c)
{
	return v[(int)c];
}


const char *color2str(Color color){
	switch (color) {
	case Color::OFF: return "Off";
	case Color::RED: return "Red";
	case Color::YELLOW: return "Yellow";
	case Color::GREEN: return "Green";
	case Color::CYAN: return "Cyan";
	case Color::BLUE: return "Blue";
	case Color::MAGENDA: return "Magenda";
	case Color::WHITE: return "White";
	case Color::ORANGE: return "Orange";
	default: return "UnknownColor";
	}
}


void getAllColorSet(ColorSet &set) {
	for (int i = 0; i < NUM_COLOR; ++i) {
		set.v[i] = true;
	}
}

void getNonOffColorSet(ColorSet &set) {
	for (int i = 0; i < NUM_COLOR; ++i) {
		set.v[i] = true;
	}
	set.v[(int)Color::OFF] = false;
}

Color randomColor() {
	ColorSet set;
	getAllColorSet(set);
	Color ret;
	myassert(set.randomPick(ret));
	return ret;
}

Color randomNonOffColor() {
	ColorSet set;
	getNonOffColorSet(set);
	Color ret;
	myassert(set.randomPick(ret));
	return ret;
}

uint8_t color2byte(Color color) {
	return (int)color;
}


bool byte2color(uint8_t b, Color & color)
{
    switch (b) {
    case 0:
        color = Color::OFF;
        return true;
    case 1:
        color = Color::RED;
        return true;
    case 2:
        color = Color::YELLOW;
        return true;
    case 3:
        color = Color::GREEN;
        return true;
    case 4:
        color = Color::CYAN;
        return true;
    case 5:
        color = Color::BLUE;
        return true;
    case 6:
        color = Color::MAGENDA;
        return true;
    case 7:
        color = Color::WHITE;
        return true;
    default:
        return false;
    }
}


void getColorDigitalValue(Color c, int&r, int &g, int &b) {
	switch (c) {
	case Color::OFF:
		r = 0; g = 0; b = 0;
		break;
	case Color::RED:
		r = 255; g = 0; b = 0;
		break;
	case Color::YELLOW:
		r = 255; g = 255; b = 0;
		break;
	case Color::GREEN:
		r = 0; g = 255; b = 0;
		break;
	case Color::CYAN:
		r = 0; g = 255; b = 255;
		break;
	case Color::BLUE:
		r = 0; g = 0; b = 255;
		break;
	case Color::MAGENDA:
		r = 255; g = 0; b = 255;
		break;
	case Color::ORANGE:
		r = 255; g = 128; b = 0;
		break;
	}
}