#include "platform.h"
#include "timers.h"


static TimerState state[NUM_TIMERS] = { TimerState::IDLE };
static uint64_t ringTime[NUM_TIMERS] = { 0 };
static uint64_t crtTime = 0;


TimerState getTimerState(uint8_t tid)
{
    return state[tid];
}

bool startTimer(uint8_t tid, uint64_t span)
{
    if (state[tid] == TimerState::RUNNING) return false;
    state[tid] = TimerState::RUNNING;
    ringTime[tid] = crtTime + span;
    return true;
}

void forceStartTimer(uint8_t tid, uint64_t span)
{
    state[tid] = TimerState::RUNNING;
    ringTime[tid] = crtTime + span;
}

void StopTimer(uint8_t tid)
{
    if (state[tid] == TimerState::RUNNING)
        state[tid] = TimerState::IDLE;
}

void refreshTimers(TimerSet &ret)
{
	crtTime = millis();
    for (uint8_t i = 0;i < NUM_TIMERS;++i) {
		ret.v[i] = (state[i] == TimerState::RUNNING&&ringTime[i] <= crtTime);
		state[i] = (state[i] == TimerState::RUNNING&&ringTime[i]>crtTime) ? TimerState::RUNNING : TimerState::IDLE;
    }
}

bool TimerSet::has(uint8_t timerId) {
	return v[timerId];
}

uint64_t getCurrentTime() {
	return crtTime;
}