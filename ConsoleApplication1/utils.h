#pragma once
#include "config.h"
#include "colors.h"


using namespace std;

bool int2str(int x, char buf[], int len);
bool byte2hex(uint8_t b, char buf[], int len);

struct BlockSet {
	bool v[NUM_BLOCK];
	bool empty();
	bool randomPick(uint8_t &ret);
	bool erase(uint8_t blockId);
	void fillAll();
	void clear();
	int size();
	void insert(uint8_t val);
	bool has(uint8_t blockId);
};

void copyBlockSet(const BlockSet &from, BlockSet &to);

struct ColorPattern {
	Color colors[NUM_BLOCK];
	void setAllToOFF();
	bool set(uint8_t blockId, Color color);
};

void copyColorPattern(const ColorPattern &from, ColorPattern &to);
void getAllBlockSet(BlockSet &set);
bool blockSetEqual(const BlockSet &bs1, const BlockSet &bs2);