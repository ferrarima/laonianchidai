#pragma once
using namespace std;


enum class LogLevel {
    ERROR = 0,  //error
    WARNING,    //error, warning
    INFO,       //error, warning, info
    DEBUG,      //error, warning, info, debug
};

void setLogLevel(LogLevel lv);
void logError(const char *str);
void logWarning(const char *str);
void logInfo(const char *str);
void logDebug(const char *str);
void logError(int);
void logWarning(int);
void logInfo(int);
void logDebug(int);
