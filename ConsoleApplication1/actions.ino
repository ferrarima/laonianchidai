#include "utils.h"
#include "timers.h"
#include "actions.h"
#include "myassert.h"
using namespace std;


const char* type2str(ActionType type) {
	switch (type) {
	case ActionType::SET_ONE_LIGHT: return "SetOneLight";
	case ActionType::SET_ALL_LIGHTS: return "SetAllLight";
	case ActionType::PLAY_MUSIC: return "PlaySound";
	case ActionType::MUTE: return "MUTE";
	case ActionType::FORCE_START_TIMER: return "ForceStartTimer";
	case ActionType::STOP_TIMER: return "StopTimer";
	default: return "UnknownAction";
	}
}

Action::Action()
{
}

Action::Action(ActionType type, const ActionArgs &args)
{
    Action::type = type;
  	for (int i = 0; i < ARGC; ++i) {
		    Action::args[i] = args.v[i];
	  }
}

void Action::log(LogLevel lv)
{
	char argstr[3 * ARGC];
	for (int i = 0; i < ARGC; ++i) {
		byte2hex(args[i], &argstr[3 * i], 3);
		if (i != ARGC - 1) argstr[3 * i + 2] = ' ';
	}
	switch (lv) {
	case LogLevel::DEBUG:
		logDebug("");
		logInfo("**********************************");
		logDebug(type2str(type));
		logDebug(argstr);
		logInfo("**********************************");
		logDebug("");
		break;
	case LogLevel::INFO:
		logInfo("");
		logInfo("**********************************");
		logInfo(type2str(type));
		logInfo(argstr);
		logInfo("**********************************");
		logInfo("");
		break;
	case LogLevel::WARNING:
		logWarning("");
		logInfo("**********************************");
		logWarning(type2str(type));
		logWarning(argstr);
		logInfo("**********************************");
		logWarning("");
		break;
	case LogLevel::ERROR:
		logError("");
		logInfo("**********************************");
		logError(type2str(type));
		logError(argstr);
		logInfo("**********************************");
		logError("");
		break;
	}

}

Action createOneLightAction(uint8_t blockId, Color color)
{
	uint8_t code = color2byte(color);
	ActionArgs args; args.init();
    args.v[0] = blockId;
    args.v[1] = code;
    return Action(ActionType::SET_ONE_LIGHT, args);
}

Action createAllLightsAction(const ColorPattern &cp)
{
	ActionArgs args; args.init();
    for (int i = 0;i < NUM_BLOCK; ++i) {
		uint8_t code = color2byte(cp.colors[i]);
        args.v[i] = code;
    }
    return Action(ActionType::SET_ALL_LIGHTS, args);
}

Action createSoundAction(Sound sound)
{
	ActionArgs args; args.init();
    uint8_t code=sound2byte(sound);
    args.v[0] = code;
    return Action(ActionType::PLAY_MUSIC, args);
}

Action createMuteAction()
{
	ActionArgs args;
    return Action(ActionType::MUTE, args);
}

Action createForceStartTimerAction(uint8_t timerId, uint64_t span)
{
	ActionArgs args; args.init();
    args.v[0] = timerId;
    args.v[1] = span >> 56;
    args.v[2] = span >> 48;
    args.v[3] = span >> 40;
    args.v[4] = span >> 32;
    args.v[5] = span >> 24;
    args.v[6] = span >> 16;
    args.v[7] = span >> 8;
    args.v[8] = span;
    return Action(ActionType::FORCE_START_TIMER, args);
}

Action createStopTimerAction(uint8_t timerId)
{
	ActionArgs args; args.init();
    args.v[0] = timerId;
    return Action(ActionType::STOP_TIMER, args);
}

bool execute(const Action & action, IO &io)
{
	Color color;
    uint8_t timerId;
    uint64_t span = 0;
	Sound sound;
    switch (action.type) {
    case ActionType::FORCE_START_TIMER:
        timerId = action.args[0];
        span += action.args[1] * 0x100000000000000ULL;
        span += action.args[2] * 0x1000000000000ULL;
        span += action.args[3] * 0x10000000000ULL;
        span += action.args[4] * 0x100000000ULL;
        span += action.args[5] * 0x1000000ULL;
        span += action.args[6] * 0x10000ULL;
        span += action.args[7] * 0x100ULL;
        span += action.args[8] * 0x1ULL;
        forceStartTimer(timerId, span);
        break;
    case ActionType::STOP_TIMER:
        timerId = action.args[0];
        StopTimer(timerId);
        break;
    case ActionType::PLAY_MUSIC:
		myassert(byte2sonud(action.args[0], sound));
		io.playSound(sound);
        break;
    case ActionType::SET_ONE_LIGHT:
		byte2color(action.args[1], color);
		io.setLight(action.args[0], color);
        break;
    case ActionType::SET_ALL_LIGHTS:
		for (int i = 0; i < NUM_BLOCK; ++i) {
			byte2color(action.args[i], color);
			io.setLight(i, color);
		}
        break;
    case ActionType::MUTE:
		io.mute();
        break;
    }
    return false;
}


const int QSIZE = 10;
static Action actionQueue[QSIZE];
static int qload = 0;
void addToActionQue(Action &action) {
	if (qload == QSIZE) {
		logWarning("Action queue is full. Some action is discarded.");
		return;
	}
	actionQueue[qload++] = action;
}

void cleanActionQue(IO &io) {
	for (int i = 0; i < qload; ++i) {
		execute(actionQueue[i], io);
	}
	qload = 0;
}


void ActionArgs::init() {
	for (int i = 0; i < ARGC; ++i) {
		v[i] = 0xFF;
	}
}
