#pragma once
#include "colors.h"
#include "sounds.h"
#include "logging.h"
#include "io.h"


using namespace std;


enum class ActionType {
    SET_ALL_LIGHTS,
    SET_ONE_LIGHT,
    PLAY_MUSIC,
    MUTE,
    FORCE_START_TIMER,
    STOP_TIMER,
};

const char* type2str(ActionType type);


const int ARGC = 10;


struct ActionArgs {
	uint8_t v[ARGC];
	void init();
};

class Action {
public:
    ActionType type;
    uint8_t args[ARGC];
	Action();
    Action(ActionType type, const ActionArgs &args);
    void log(LogLevel lv);
};


Action createOneLightAction(uint8_t blockId, Color color);
Action createAllLightsAction(const ColorPattern &cp);
Action createSoundAction(Sound sound);
Action createMuteAction();
Action createForceStartTimerAction(uint8_t timerId, uint64_t span);
Action createStopTimerAction(uint8_t timerId);

bool execute(const Action &action, IO &io);
void cleanActionQue(IO &io);
void addToActionQue(Action &action);
