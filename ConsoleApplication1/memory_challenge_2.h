#pragma once
#include "machines.h"
#include "utils.h"


/**
 *  Challenge your memory Lv. 2.
 *
 *  Two blocks are highlighted, one after the other.
 *  One is colored x, the other y.
 *  Then all the blocks are colored, only one colored with x and one with y.
 *  Player should first touch the block with color x,
 *  then touch the one with color y.
 *
 *  Timeline (2 correct):
 *  | PreGap | Hint 1 | InGap 1 | Hint 2 | InGap 2 | Answer 1 | Answer 2 | Wow |  SufGap |
 *
 *  Timeline (1 correct):
 *  | PreGap | Hint 1 | InGap 1 | Hint 2 | InGap 2 | Answer 1 | Answer 2 | Oops | SufGap |
 *
 *  Timeline (0 correct):
 *  | PreGap | Hint 1 | InGap 1 | Hint 2 | InGap 2 | Answer 1 | Oops | SufGap |
 */
class MemoryChallengeLv2 : public Machine {
private:
    static const int PRE_GAP_DURATION = 500;
    static const int HINT_DURATION = 2000;
    static const int IN_GAP_1_DURATION = 1000;
    static const int IN_GAP_2_DURATION = 1000;
    static const int ANS_TIME_LIMIT = 8000;
    static const int SUF_GAP_DURATION = 500;
    static const int OOPS_DURATION = 1000;
    static const int WOW_DURATION = 1000;

    enum StateCode {
        IN_PRE_GAP,
        HINT_1_STARTED,
        IN_IN_GAP_1,
        HINT_2_STARTED,
        IN_IN_GAP_2,
        WTING_ANSWER_1,
        WTING_ANSWER_2,
        IN_OOPS,
        IN_WOW,
        IN_SUF_GAP,
    };
    StateCode state;
    uint8_t     hintBlock1;
    uint8_t     hintBlock2;
    uint8_t     touchAnswer1;
    uint8_t     touchAnswer2;
    Color       hintColor1;
    Color       hintColor2;
    ColorPattern choices;
    SilentGapMachine gapMachine;
    WowMachine wowMachine;
    OopsMachine oopsMachine;
    SingleHintMachine hintMachine;
    SingleSelectMachine selMachine;
    bool succ;
    void init();
public:
    // Inherited via Machine
    void restart(uint64_t timestamp) override;
    void handle(const Event & evt) override;
    bool succeeded();
};