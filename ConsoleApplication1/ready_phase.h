#pragma once
#include "machines.h"
#include "blink.h"

class ReadyPhaseMachine : public Machine {
private:
	const static int REPEAT = 5;
	int counter;
	BlinkMachine blinkMachine;
public:
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
	bool isReady();
};