#pragma once

#include "machines.h"

class Stage3Machine : public Machine {
public:
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
	bool passed();
};
