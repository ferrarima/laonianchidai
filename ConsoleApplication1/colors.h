#pragma once
#include <stdint.h>
using namespace std;


const int NUM_COLOR=9;

enum class Color {
    OFF=0,
    RED,
    YELLOW,
    GREEN,
    CYAN,
    BLUE,
    MAGENDA,
    WHITE,
	ORANGE,
};


const char *color2str(Color color);
uint8_t color2byte(Color color);
bool byte2color(uint8_t b, Color &color);


struct ColorSet {
    bool v[NUM_COLOR];
    bool empty();
    bool randomPick(Color &ret);
	bool erase(Color c);
	int size();
	void insert(Color color);
	bool has(Color c);
};


void getAllColorSet(ColorSet &set);
void getNonOffColorSet(ColorSet &set);
Color randomColor();
Color randomNonOffColor();

void getColorDigitalValue(Color c, int&vr, int &vg, int &vb);