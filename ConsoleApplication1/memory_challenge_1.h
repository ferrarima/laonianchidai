#pragma once
#include "machines.h"


/**Memory challenge Lv. 1.
*
* One block is highlighted with color x for a few seconds.
* Then all the blocks are colored, only one colored with x.
* Player should touch the block colored with x,
* within a few seconds.
*
*  Timeline (correct):
*  | PreGap | Hint | InGap | Answer | Wow | SufGap |
*
*  Timeline (incorrect):
*  | PreGap | Hint | InGap | Answer | Oops | SufGap |
*/
class MemoryChallengeLv1Machine : public Machine {
private:
    static const int PRE_GAP_DURATION = 500;
    static const int HINT_DURATION = 2000;
    static const int IN_GAP_DURATION = 1000;
    static const int ANS_TIME_LIMIT = 2000;
    static const int SUF_GAP_DURATION = 500;

    enum class StateCode {
        IN_PRE_GAP,
        HINT_STARTED,
        IN_IN_GAP,
        WTING_ANSWER,
        IN_WOW,
        IN_OOPS,
        IN_SUF_GAP,
        TERMINATED,
    };

	StateCode state;
	uint8_t hintBlock;
	Color hintColor;
	uint8_t touchAnswer;
	ColorPattern choices;
    //Color       choiceColors[NUM_BLOCK];
    bool        correct;

    SingleHintMachine hintMachine;
    SingleSelectMachine selMachine;
    SilentGapMachine gapMachine;
    WowMachine wowMachine;
    OopsMachine oopsMachine;

public:
    void restart(uint64_t timestamp) override;
    void handle(const Event & evt) override;
	bool succeeded();
};
