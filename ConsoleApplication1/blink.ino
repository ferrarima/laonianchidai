#include "blink.h"
#include "myassert.h"


void BlinkMachine::config(Color c)
{
	color = c;
}

void BlinkMachine::restart(uint64_t timestamp)
{
	ColorPattern cp;
	for (int i = 0; i < NUM_BLOCK; ++i) cp.set(i, color);

	auto act1 = createAllLightsAction(cp);
	addToActionQue(act1);
	
	auto act2 = createForceStartTimerAction(0, 100);
	addToActionQue(act2);

	firstPhase = true;
	gstate = GeneralState::RUNNING;
}

void BlinkMachine::handle(const Event & evt)
{
	myassert(gstate==GeneralState::RUNNING, "BlinkMachine not running but handling");
	if (evt.type == EventType::EVT_TIMER && evt.arg == 0) {
		if (firstPhase) {
			firstPhase = false;

			ColorPattern cp;cp.setAllToOFF();

			auto act1 = createAllLightsAction(cp);
			addToActionQue(act1);

			auto act2 = createForceStartTimerAction(0, 100);
			addToActionQue(act2);

		}
		else {
			
			gstate = GeneralState::IDLE;
		}
	}
}
