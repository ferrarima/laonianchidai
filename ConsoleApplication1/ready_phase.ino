#include "ready_phase.h"
#include "actions.h"
#include "myassert.h"

void ReadyPhaseMachine::restart(uint64_t timestamp)
{
	counter = 0;
	blinkMachine.config(Color::RED);
	blinkMachine.restart(timestamp);
	gstate = GeneralState::RUNNING;
}

void ReadyPhaseMachine::handle(const Event & evt)
{
	myassert(gstate == GeneralState::RUNNING, "ReadyMachine not running but handling");
	blinkMachine.handle(evt);
	if (blinkMachine.getGeneralState() == GeneralState::IDLE) {
		counter++;
		if (counter == REPEAT)
			gstate = GeneralState::IDLE;
		else {
			blinkMachine.config(Color::RED);
			blinkMachine.restart(evt.timestamp);
		}
	}
	logDebug("machine ready: ");
	logDebug((int)gstate);
	logDebug("");
}

bool ReadyPhaseMachine::isReady()
{
	return true;
}
