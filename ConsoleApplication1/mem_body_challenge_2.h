#pragma once
#include "machines.h"
#include "multi_select.h"


class MemBodyChallengeLv2 : public Machine {
private:
	static const uint64_t PRE_GAP_DURATION = 500;
	static const uint64_t HINT_DURATION = 2000;
	static const uint64_t INGAP1_DURATION = 500;
	static const uint64_t INGAP2_DURATION = 2000;
	static const uint64_t ANS_TIME_LIMIT = 8000;
	static const uint64_t POST_GAP_DURATION = 500;
	enum class StateCode {
		IN_PRE_GAP,
		IN_MULTI_HINT_1,
		IN_IN_GAP_1,
		IN_MULTI_HINT_2,
		IN_IN_GAP_2,
		IN_MULTI_SELECT_1,
		IN_MULTI_SELECT_2,
		IN_OOPS,
		IN_WOW,
		IN_POST_GAP,
	};
	Color hintColor1;
	Color hintColor2;
	ColorPattern hint1;
	ColorPattern hint2;
	ColorPattern choices;
	BlockSet touchAns1;
	BlockSet touchAns2;
	uint8_t p11, p12, p21, p22;
	bool correct;

	StateCode state;
	SilentGapMachine gapMachine;
	//MultiSelectMachine selMachine;
	SingleSelectMachine selMachine;
	MultiHintMachine hintMachine;
	WowMachine wowMachine;
	OopsMachine oopsMachine;
public:
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
	bool succeeded();
};
