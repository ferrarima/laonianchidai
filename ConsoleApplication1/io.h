#pragma once
#include "sounds.h"
#include "config.h"
#include "colors.h"


class IO {
public:
	virtual void init()=0;
	virtual void playSound(Sound sound)=0;
	virtual void mute()=0;
	virtual void setLight(uint8_t blockId, Color color)=0;

	virtual void sample()=0;
	virtual bool touchHappend(uint8_t i)=0;
	virtual bool hasPlayer()=0;
};


class VibIO :public IO {
private:
	static const int SOUND_PIN_NUM = 10;
	int weightPin;
	int soundPin[SOUND_PIN_NUM];
	int touchCrt[NUM_BLOCK];
	int touchAcc[NUM_BLOCK];
	int touchPin[NUM_BLOCK];
	int lightPin[NUM_BLOCK][3];
	int kgCrt;
public:
	virtual void init() override;
	virtual void playSound(Sound sound) override;
	virtual void mute() override;
	virtual void setLight(uint8_t blockId, Color color) override;
	virtual void sample() override;
	virtual bool touchHappend(uint8_t i) override;
	virtual bool hasPlayer() override;
};