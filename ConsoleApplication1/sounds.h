#pragma once
using namespace std;

enum class Sound {
    WOW,
    OOPS,
	IDLE_BROWSING,
};

uint8_t sound2byte(Sound sound);
bool byte2sonud(uint8_t b, Sound &sound);

bool playSound(Sound sound);
bool mute();
