#include "game_results.h"
#include "myassert.h"

void GameResultMachine::restart(uint64_t timestamp)
{
	counter = 0;
	blinkMachine.config(Color::WHITE);
	blinkMachine.restart(timestamp);
	gstate = GeneralState::RUNNING;
}

void GameResultMachine::handle(const Event & evt)
{
	myassert(gstate==GeneralState::RUNNING, "ResultMachine not running but handling");
	blinkMachine.handle(evt);
	if (blinkMachine.getGeneralState()==GeneralState::IDLE) {
		counter++;
		if (counter==REPEAT)
			gstate = GeneralState::IDLE;
		else {
			blinkMachine.config(Color::WHITE);
			blinkMachine.restart(evt.timestamp);
		}
	}
	logDebug("machine result: ");
	logDebug((int)gstate);
	logDebug("");
}

