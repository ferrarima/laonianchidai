#include "mem_body_challenge_2.h"
#include "myassert.h"


void MemBodyChallengeLv2::restart(uint64_t timestamp)
{
	ColorSet cs; getNonOffColorSet(cs);
	myassert(!cs.empty()); cs.randomPick(hintColor1); cs.erase(hintColor1);
	myassert(!cs.empty()); cs.randomPick(hintColor2); cs.erase(hintColor2);

	BlockSet bs1; bs1.fillAll();
	uint8_t b11, b12;
	myassert(!bs1.empty()); bs1.randomPick(b11); bs1.erase(b11);
	myassert(!bs1.empty()); bs1.randomPick(b12); bs1.erase(b12);
	hint1.setAllToOFF();
	hint1.colors[b11] = hint1.colors[b12] = hintColor1;
	
	BlockSet bs2; bs2.fillAll();
	uint8_t b21, b22;
	myassert(!bs2.empty()); bs2.randomPick(b21); bs2.erase(b21);
	myassert(!bs2.empty()); bs2.randomPick(b22); bs2.erase(b22);
	hint2.setAllToOFF();
	hint2.colors[b21] = hint2.colors[b22] = hintColor2;

	BlockSet bs; bs.fillAll();
	myassert(!bs.empty()); bs.randomPick(p11); bs.erase(p11);
	myassert(!bs.empty()); bs.randomPick(p12); bs.erase(p12);
	myassert(!bs.empty()); bs.randomPick(p21); bs.erase(p21);
	myassert(!bs.empty()); bs.randomPick(p22); bs.erase(p22);
	//p11 = 1; p12 = 0; p21 = 2; p22 = 4;

	for (int i = 0; i < NUM_BLOCK; ++i) {
		if (i == p11 || i == p12) {
			choices.set(i, hintColor1);
		}
		else if (i == p21 || i == p22) {
			choices.set(i, hintColor2);
		}
		else {
			Color tmp;
			myassert(!cs.empty()); cs.randomPick(tmp); cs.erase(tmp);
			choices.set(i, tmp);
		}
	}

	touchAns1.clear(); touchAns1.insert(p11); touchAns1.insert(p12);
	touchAns2.clear(); touchAns2.insert(p21); touchAns2.insert(p22);


	gapMachine.setDuration(PRE_GAP_DURATION);
	gapMachine.restart(timestamp);
	state = StateCode::IN_PRE_GAP;
	gstate = GeneralState::RUNNING;
}

void MemBodyChallengeLv2::handle(const Event & evt)
{
	myassert(gstate == GeneralState::RUNNING, "MemBody2 receives events while not running.");
	switch (state) {
	case StateCode::IN_PRE_GAP:
		gapMachine.handle(evt);
		if (gapMachine.getGeneralState() == GeneralState::IDLE) {
			
			hintMachine.config(hint1, HINT_DURATION);
			hintMachine.restart(evt.timestamp);
			state = StateCode::IN_MULTI_HINT_1;
		}
		break;
	case StateCode::IN_MULTI_HINT_1:
		hintMachine.handle(evt);
		if (hintMachine.getGeneralState() == GeneralState::IDLE) {
			gapMachine.setDuration(INGAP1_DURATION);
			gapMachine.restart(evt.timestamp);
			state = StateCode::IN_IN_GAP_1;
		}
		break;
	case StateCode::IN_IN_GAP_1:
		gapMachine.handle(evt);
		if (gapMachine.getGeneralState() == GeneralState::IDLE) {
			hintMachine.config(hint2, HINT_DURATION);
			hintMachine.restart(evt.timestamp);
			state = StateCode::IN_MULTI_HINT_2;
		}
		break;
	case StateCode::IN_MULTI_HINT_2:
		hintMachine.handle(evt);
		if (hintMachine.getGeneralState() == GeneralState::IDLE) {
			gapMachine.setDuration(INGAP2_DURATION);
			gapMachine.restart(evt.timestamp);
			state = StateCode::IN_IN_GAP_2;
		}
		break;
	case StateCode::IN_IN_GAP_2:
		gapMachine.handle(evt);
		if (gapMachine.getGeneralState() == GeneralState::IDLE) {
			BlockSet allEnabled; allEnabled.fillAll();
			selMachine.config(choices, allEnabled, ANS_TIME_LIMIT);
			selMachine.restart(evt.timestamp);
			state = StateCode::IN_MULTI_SELECT_1;
		}
		break;
	case StateCode::IN_MULTI_SELECT_1:
		selMachine.handle(evt);
		if (selMachine.getGeneralState() == GeneralState::IDLE) {
			uint64_t timeUsed = selMachine.getTimeUsed();
			//BlockSet ans1; selMachine.getSelection(ans1);
			uint8_t sid = selMachine.getSelection();
			if (touchAns1.has(sid)) {
				choices.set(p11, Color::OFF);
				choices.set(p12, Color::OFF);
				logDebug("interesting");
				//assert timeUsed < ANS_TIME_LIMIT
				BlockSet enabled; enabled.fillAll(); enabled.erase(p11); enabled.erase(p12);
				selMachine.config(choices, enabled, ANS_TIME_LIMIT - timeUsed);
				selMachine.restart(evt.timestamp);
				state = StateCode::IN_MULTI_SELECT_2;
			}
			else {
				correct = false;
				oopsMachine.restart(evt.timestamp);
				state = StateCode::IN_OOPS;
			}
		}
		break;
	case StateCode::IN_MULTI_SELECT_2:
		selMachine.handle(evt);
		if (selMachine.getGeneralState() == GeneralState::IDLE) {
			//BlockSet ans2; selMachine.getSelection(ans2);
			uint8_t sid = selMachine.getSelection();
			if (touchAns2.has(sid)) {
				//assert timeUsed < time limit
				auto act1 = createOneLightAction(p21, Color::OFF);
				addToActionQue(act1);
				auto act2 = createOneLightAction(p22, Color::OFF);
				addToActionQue(act2);
				correct = true;
				wowMachine.restart(evt.timestamp);
				state = StateCode::IN_WOW;
			}
			else {
				correct = false;
				oopsMachine.restart(evt.timestamp);
				state = StateCode::IN_OOPS;
			}
		}
		break;
	case StateCode::IN_OOPS:
		oopsMachine.handle(evt);
		if (oopsMachine.getGeneralState() == GeneralState::IDLE) {
			gapMachine.setDuration(POST_GAP_DURATION);
			gapMachine.restart(evt.timestamp);
			state = StateCode::IN_POST_GAP;
		}
		break;
	case StateCode::IN_WOW:
		wowMachine.handle(evt);
		if (wowMachine.getGeneralState() == GeneralState::IDLE) {
			gapMachine.setDuration(POST_GAP_DURATION);
			gapMachine.restart(evt.timestamp);
			state = StateCode::IN_POST_GAP;
		}
		break;
	case StateCode::IN_POST_GAP:
		gapMachine.handle(evt);
		if (gapMachine.getGeneralState() == GeneralState::IDLE) {
			gstate = GeneralState::IDLE;
		}
		break;
	}
}

bool MemBodyChallengeLv2::succeeded()
{
	return false;
}
 