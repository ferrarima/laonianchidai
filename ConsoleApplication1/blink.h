#pragma once

#include "machines.h"

class BlinkMachine : public Machine {
private:
	bool firstPhase;
	Color color;
public:
	void config(Color c);
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
};