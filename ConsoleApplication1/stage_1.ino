#include "stage_1.h"
#include "myassert.h"


void Stage1Machine::restart(uint64_t timestamp)
{
	score = 0;
	counter = 0;
	chaManage.restart(timestamp);
	gstate = GeneralState::RUNNING;
}

void Stage1Machine::handle(const Event & evt)
{
	myassert(gstate == GeneralState::RUNNING, "Stg1Machine not running but handle");
	chaManage.handle(evt);
	if (chaManage.getGeneralState() == GeneralState::IDLE) {
		if (chaManage.succeeded()) {
			score += 1;
		}
		counter++;
		if (counter == TOTAL) {
			gstate = GeneralState::IDLE;
		}
		else {
			chaManage.restart(evt.timestamp);
		}
	}
	logDebug("machine stage1: ");
	logDebug((int)counter);
	logDebug("");
}

bool Stage1Machine::passed()
{
	return true;
}
