#pragma once

#include "machines.h"
#include "utils.h"


class MultiSelectMachine : public Machine {
private:
	static const uint64_t THRESHOLD = 400;
	enum class StateCode {
		IN_1ST_SEL,
		IN_NTH_SEL,
	};
	StateCode state;
	uint64_t timeLimit;
	ColorPattern choices;
	SingleSelectMachine selMachine;

	BlockSet enabled;
	BlockSet selection;
	uint64_t timeUsed;
	void init();
public:
	void config(const ColorPattern &cp, const BlockSet &enabled, uint64_t timeLimit);
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
	void getSelection(BlockSet &ret);
	uint64_t getTimeUsed();
};