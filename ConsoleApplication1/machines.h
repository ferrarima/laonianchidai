#pragma once
#include "config.h"
#include "actions.h"
#include "events.h"
#include "utils.h"
using namespace std;


enum class GeneralState {
    IDLE,
    RUNNING,
    ABORTED,
};


class Machine {
protected:
    GeneralState gstate;
public:
    virtual void restart(uint64_t timestamp)=0;
    virtual void handle(const Event &evt)=0;
    GeneralState getGeneralState();
	void terminate();
};


/**Playing sound effect "WOW".
 * Can be used when player touches a correct block.
 */
class WowMachine : public Machine {
private:
    static const int WOW_DURATION = 500;
	static const int TIMER_ID = 0;
public:
    // Inherited via Machine
    virtual void restart(uint64_t timestamp) override;
    virtual void handle(const Event & evt) override;
};


/**Playing sound effect "OOPS".
 * Can be used when player touches an incorrect block.
 */
class OopsMachine : public Machine {
private:
	static const int TIMER_ID = 0;
    static const int OOPS_DURATION = 500;
public:
    // Inherited via BaseState
    virtual void restart(uint64_t timestamp) override;
    virtual void handle(const Event & evt) override;
};


/**A few seconds of silence, all sounds and lights off.
 * Can be used as gaps between different phases of the game.
 *
 * \note    To change duration for next run,
 *          call setDuration() right before next restart().
 */
class SilentGapMachine : public Machine {
private:
	static const int TIMER_ID = 0;
	uint64_t duration;
public:
    void setDuration(uint64_t duration);
    virtual void restart(uint64_t timestamp) override;
    virtual void handle(const Event & evt) override;
};


/**Display one color in one block for a while.
 * Can be used in almost every type of challenge.
 *
 * \note    To change color for next run,
 *          call setColor() right before next restart().
 */
class SingleHintMachine : public Machine {
private:
	static const int TIMER_ID = 0;
	Color hintColor;
    uint8_t hintBlock;
    uint64_t duration;
public:
    void config(uint8_t blockId, Color color, uint64_t duration);
    virtual void restart(uint64_t timestamp) override;
    virtual void handle(const Event & evt) override;
};


/**Display a pattern, listen for a touch.
 *
 * You can specify the set of blocks you are interested in.
 * Touch events on the other blocks will be ignored.
 */
class SingleSelectMachine : public Machine {
private:
	static const int TIMER_ID = 0;
	ColorPattern choices;
	BlockSet enabled;
	uint64_t timeLimit;
    bool selected;
    uint8_t selection;
    uint64_t startTime;
    uint64_t timeUsed;
public:
    void config(const ColorPattern &cp, const BlockSet &enabled, uint64_t timeLimit);
    virtual void restart(uint64_t timestamp) override;
    virtual void handle(const Event & evt) override;
    bool getSelected();
    uint8_t getSelection();
    uint64_t getTimeUsed();
};


class MultiHintMachine : public Machine {
private:
	static const uint8_t TIMER_ID = 0;
	ColorPattern hint;
	uint64_t duration;
public:
	void config(const ColorPattern &hint, uint64_t duration);
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
};
