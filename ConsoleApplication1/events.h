#pragma once
#include "logging.h"
#include "io.h"

enum class EventType {
    EVT_TIMER,
    EVT_TOUCH,
    EVT_PLAYER,
};

const char* et2str(EventType et);


class Event {
public:
    uint64_t timestamp;
    EventType type;
    uint8_t arg;
    Event(uint64_t timestamp, EventType type, uint8_t arg);
	void log(LogLevel lv);
};
void refreshEventService(IO &io);
bool hasNextEvent();
Event nextEvent();
