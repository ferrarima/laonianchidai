#include "stage_2.h"
#include "myassert.h"


void Stage2Machine::restart(uint64_t timestamp)
{
	score = 0;
	counter = 0;
	chaMachine.restart(timestamp);
	gstate = GeneralState::RUNNING;
}

void Stage2Machine::handle(const Event & evt)
{
	myassert(gstate == GeneralState::RUNNING, "Stg2Machine not running but handle");
	chaMachine.handle(evt);
	if (chaMachine.getGeneralState() == GeneralState::IDLE) {
		if (chaMachine.succeeded()) {
			score += 1;
		}
		counter++;
		if (counter == TOTAL) {
			gstate = GeneralState::IDLE;
		}
		else {
			chaMachine.restart(evt.timestamp);
		}
	}
	logDebug("machine stage2: ");
	logDebug((int)counter);
	logDebug("");

}

bool Stage2Machine::passed()
{
	return true;
}
