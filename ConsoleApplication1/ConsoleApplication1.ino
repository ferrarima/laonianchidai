#include "logging.h"
#include "colors.h"
#include "platform.h"
#include "myassert.h"
#include "utils.h"
#include "timers.h"
#include "events.h"
#include "actions.h"
#include "io.h"
#include "machines.h"
#include "multi_select.h"
#include "memory_challenge_1.h"
#include "memory_challenge_2.h"
#include "mem_body_challenge_2.h"
#include "publish.h"
using namespace std;

VibIO io;
MainMachine machine;

void setup() {
	randomSeed(7654);
    Serial.begin(9600);
    setLogLevel(LogLevel::DEBUG);
//    forceStartTimer(2,2000);
	io.init();
	//ColorPattern cp;
	//BlockSet enabled; enabled.fillAll();
	//machine.config(cp, enabled, 5000);
  logDebug("*");
	machine.restart(millis());
  cleanActionQue(io);
  logDebug("*");
}


void loop() {
	io.sample();
	refreshEventService(io);
	while (hasNextEvent()) {
		auto evt = nextEvent();
		evt.log(LogLevel::INFO);
		machine.handle(evt);
		if (machine.getGeneralState() == GeneralState::IDLE) {
			logWarning("machine stopped");
			machine.restart(evt.timestamp);
			//BlockSet selection;
			//machine.getSelection(selection);
			//int x = selection.size();
			//logInfo(x);
			//ColorPattern cp;
			//BlockSet enabled; enabled.fillAll();
			//machine.config(cp, enabled, 5000);
			machine.restart(evt.timestamp);
		}
		cleanActionQue(io);
	}
}
