#include "utils.h"
#include "config.h"
#include "platform.h"

//int min(int x, int y){
//    if (x<y) return x;return y;
//}

char hexDigitOf(uint8_t x) {
	if (x < 10) return x + 48;
	return x + 55;
}

bool byte2hex(uint8_t b, char buf[], int len) {
	if (buf == NULL || len < 3) return false;
	buf[0] = hexDigitOf(b/16);
	buf[1] = hexDigitOf(b&0x0f);
	buf[2] = 0;
	return true;
}


int strlenofpos(int x) {
	int ret = 0;
	while (x > 0) {
		++ret;
		x /= 10;
	}
	return ret;
}



int strlenofint(int x) {
	if (x == 0) return 0;
	if (x < 0) return 1 + strlenofpos(-x);
	return strlenofpos(x);
}

bool int2str(int x, char buf[], int len)
{
	if (buf == NULL) return false;
	int calced = strlenofint(x);
	if (len <= calced) return false;
	int i = 0;
	if (x < 0) {
		buf[i++] = '-';
		x = -x;
	}
	if (x == 0) {
		buf[i++] = '0';
	}
	else {
		int tmp[20];
		int tmpn = 0;
		while (x > 0) {
			tmp[tmpn++] = x % 10;
			x /= 10;
		}
		for (int j = tmpn - 1; j >= 0; --j) {
			buf[i++] = tmp[j] + 48;
		}
	}
	buf[i++] = 0;
    return true;
}

void copyBlockSet(const BlockSet & from, BlockSet & to)
{
	for (int i = 0; i < NUM_BLOCK; ++i) {
		to.v[i] = from.v[i];
	}
}

void copyColorPattern(const ColorPattern & from, ColorPattern & to)
{
	for (int i = 0; i < NUM_BLOCK; ++i) {
		to.colors[i] = from.colors[i];
	}
}

void getAllBlockSet(BlockSet &set) {
	for (int i = 0; i < NUM_BLOCK; ++i) {
		set.v[i] = i;
	}
}

bool blockSetEqual(const BlockSet & bs1, const BlockSet & bs2)
{
	for (int i = 0; i < NUM_BLOCK; ++i) {
		if (bs1.v[i] != bs2.v[i]) return false;
	}
	return true;
}


bool BlockSet::empty() {
	for (int i = 0; i < NUM_BLOCK; ++i) {
		if (v[i]) return false;
	}
	return true;
}

bool BlockSet::randomPick(uint8_t &ret) {
	if (empty()) return false;
	uint8_t tmp[NUM_BLOCK];
	int tmpn = 0;
	for (int i = 0; i < NUM_BLOCK; ++i) {
		if (v[i]) tmp[tmpn++] = i;
	}
	int idx = random(0,tmpn-1);
	ret = tmp[idx];
	return true;
}

bool BlockSet::erase(uint8_t blockId)
{
	bool ret = v[blockId];
	v[blockId] = false;
	return ret;
}

void BlockSet::fillAll()
{
	for (int i = 0; i < NUM_BLOCK; ++i) {
		v[i] = true;
	}
}

void BlockSet::clear()
{
	for (int i = 0; i<NUM_BLOCK; i++) {
		v[i] = false;
	}
}

int BlockSet::size()
{
	int ret = 0;
	for (int i = 0; i < NUM_BLOCK; i++) {
		if (v[i]) ++ret;
	}
	return ret;
}

void BlockSet::insert(uint8_t val)
{
	if (val >= NUM_BLOCK) return;
	v[val] = true;
}

bool BlockSet::has(uint8_t blockId)
{
	if (blockId >= NUM_BLOCK) return false;
	return v[blockId];
}


void ColorPattern::setAllToOFF() {
	for (int i = 0; i<NUM_BLOCK; i++) {
		colors[i] = Color::OFF;
	}
}

bool ColorPattern::set(uint8_t blockId, Color color)
{
	if (blockId >= NUM_BLOCK) return false;
	colors[blockId] = color;
	return true;
}
