#pragma once
#include "machines.h"
#include "ready_phase.h"
#include "stage_1.h"
#include "stage_2.h"
#include "stage_3.h"
#include "game_results.h"


class MainMachine : public Machine {
private:
	enum class StateCode {
		WTING,
		READY_PHASE,
		IN_STAGE_1,
		IN_STAGE_2,
		IN_STAGE_3,
		IN_RESULT,
	};
	StateCode state;
	ReadyPhaseMachine readyMachine;
	Stage1Machine stg1Machine;
	Stage2Machine stg2Machine;
	Stage1Machine stg3Machine;
	GameResultMachine resMachine;
public:
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
};

