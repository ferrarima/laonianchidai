#include "platform.h"
#include "utils.h"
#include "memory_challenge_2.h"
#include "myassert.h"

void MemoryChallengeLv2::init()
{
    hintBlock1 = random(0, NUM_BLOCK);
    hintBlock2 = random(0, NUM_BLOCK);

	ColorSet cs; getNonOffColorSet(cs);
	myassert(!cs.empty());
	cs.randomPick(hintColor1);
	cs.erase(hintColor1);

	myassert(!cs.empty());
	cs.randomPick(hintColor2);
	cs.erase(hintColor2);

	BlockSet bs; getAllBlockSet(bs); bs.erase(0); bs.erase(7);
	myassert(!bs.empty(), "MC2 machine internal error 1");
	bs.randomPick(touchAnswer1);
	//touchAnswer1 = 2;
	bs.erase(touchAnswer1);
	myassert(!bs.empty(), "MC2 machine internal error 2");
	bs.randomPick(touchAnswer2);
	//touchAnswer2 = 3;
	bs.erase(touchAnswer2);
    for (int i = 0;i < NUM_BLOCK;++i) {
        if (i == touchAnswer1) {
            choices.colors[i] = hintColor1;
        }
        else if (i == touchAnswer2) {
            choices.colors[i] = hintColor2;
        }
        else {
			myassert(!cs.empty(), "MC2 machine internal error 3");
            cs.randomPick(choices.colors[i]);
            cs.erase(choices.colors[i]);
		}
    }
}

void MemoryChallengeLv2::restart(uint64_t timestamp)
{
	init();
    
	gapMachine.setDuration(PRE_GAP_DURATION);
    gapMachine.restart(timestamp);

    gstate = GeneralState::RUNNING;
    state = IN_PRE_GAP;

}

void MemoryChallengeLv2::handle(const Event & evt)
{
    myassert(gstate == GeneralState::RUNNING);

    if (state == IN_PRE_GAP) {

        gapMachine.handle(evt);

        if (gapMachine.getGeneralState() == GeneralState::IDLE) {

            hintMachine.config(hintBlock1, hintColor1, HINT_DURATION);
            hintMachine.restart(evt.timestamp);

            state = HINT_1_STARTED;
        }
    }
    else if (state == HINT_1_STARTED) {

        hintMachine.handle(evt);

        if (hintMachine.getGeneralState() == GeneralState::IDLE) {

            gapMachine.restart(evt.timestamp);

            state = IN_IN_GAP_1;
        }
    }
    else if (state == IN_IN_GAP_1) {
        gapMachine.handle(evt);

        if (gapMachine.getGeneralState() == GeneralState::IDLE) {
            hintMachine.config(hintBlock2, hintColor2, HINT_DURATION);
            hintMachine.restart(evt.timestamp);
            state = HINT_2_STARTED;
        }
    }
    else if (state == HINT_2_STARTED) {
        hintMachine.handle(evt);

        if (hintMachine.getGeneralState() == GeneralState::IDLE) {
            gapMachine.setDuration(IN_GAP_2_DURATION);
            gapMachine.restart(evt.timestamp);

            state = IN_IN_GAP_2;
        }
    }
    else if (state == IN_IN_GAP_2) {
        gapMachine.handle(evt);

        if (gapMachine.getGeneralState() == GeneralState::IDLE) {
			BlockSet bs; bs.fillAll();
			selMachine.config(choices, bs, ANS_TIME_LIMIT);
            selMachine.restart(evt.timestamp);

            state = WTING_ANSWER_1;
        }
    }
    else if (state == WTING_ANSWER_1) {
        selMachine.handle(evt);

        if (selMachine.getGeneralState() == GeneralState::IDLE) {
            if (selMachine.getSelected() == true && selMachine.getSelection() == touchAnswer1) {
                uint64_t timeUsed = selMachine.getTimeUsed();
				choices.colors[touchAnswer1] = Color::OFF;
				BlockSet bs; bs.fillAll(); bs.erase(touchAnswer1);
				selMachine.config(choices, bs, ANS_TIME_LIMIT - timeUsed);
                selMachine.restart(evt.timestamp);

                state = WTING_ANSWER_2;
            }
            else {
                succ = false;
                oopsMachine.restart(evt.timestamp);

                state = IN_OOPS;
            }
        }
    }
    else if (state == WTING_ANSWER_2) {
        selMachine.handle(evt);

        if (selMachine.getGeneralState() == GeneralState::IDLE) {
            if (selMachine.getSelected() == true && selMachine.getSelection() == touchAnswer2) {
                succ = true;
                auto act = createOneLightAction(touchAnswer2, Color::OFF);
				addToActionQue(act);
                wowMachine.restart(evt.timestamp);

                state = IN_WOW;
            }
            else {
                succ = false;
                oopsMachine.restart(evt.timestamp);

                state = IN_OOPS;
            }
        }
    }
    else if (state == IN_OOPS) {
        oopsMachine.handle(evt);

        if (oopsMachine.getGeneralState() == GeneralState::IDLE) {
            gapMachine.setDuration(SUF_GAP_DURATION);
            gapMachine.restart(evt.timestamp);

            state = IN_SUF_GAP;
        }
    }
    else if (state == IN_WOW) {
        wowMachine.handle(evt);
        if (wowMachine.getGeneralState() == GeneralState::IDLE) {
            gapMachine.setDuration(SUF_GAP_DURATION);
            gapMachine.restart(evt.timestamp);

            state = IN_SUF_GAP;
        }
    }
    else if (state == IN_SUF_GAP) {
        gapMachine.handle(evt);
        if (gapMachine.getGeneralState() == GeneralState::IDLE) {
            gstate = GeneralState::IDLE;
        }
    }
}

bool MemoryChallengeLv2::succeeded()
{
    return succ;
}
