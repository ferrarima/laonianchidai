#include "multi_select.h"
#include "myassert.h"
#include "utils.h"


void MultiSelectMachine::init()
{
	selection.clear();
	timeUsed = 0;
}

void MultiSelectMachine::config(const ColorPattern & cp, const BlockSet &enabled, uint64_t timeLimit)
{
	copyBlockSet(enabled, MultiSelectMachine::enabled);
	copyColorPattern(cp, choices);
	MultiSelectMachine::timeLimit = timeLimit;
}

void MultiSelectMachine::restart(uint64_t timestamp)
{
	init();
	BlockSet allEnabled; allEnabled.fillAll();
	selMachine.config(choices, allEnabled, timeLimit);
	selMachine.restart(timestamp);
	state = StateCode::IN_1ST_SEL;
	gstate = GeneralState::RUNNING;
}

void MultiSelectMachine::handle(const Event & evt)
{
	myassert(gstate==GeneralState::RUNNING, "MultiSelect machine received events when not running.");
	switch (state) {
	case StateCode::IN_1ST_SEL:
		selMachine.handle(evt);
		if (selMachine.getGeneralState() == GeneralState::IDLE) {
			if (selMachine.getSelected() == false) {
				timeUsed = timeLimit;
				gstate = GeneralState::IDLE;
			}
			else {
				uint8_t sid = selMachine.getSelection();
				selection.insert(sid);
				enabled.erase(sid);
				timeUsed += selMachine.getTimeUsed();
				uint64_t specialTimeLimit = min(THRESHOLD, timeLimit - timeUsed);
				selMachine.config(choices, enabled, specialTimeLimit);
				selMachine.restart(evt.timestamp);
				state = StateCode::IN_NTH_SEL;
			}
		}
		break;
	case StateCode::IN_NTH_SEL:
		selMachine.handle(evt);
		if (selMachine.getGeneralState() == GeneralState::IDLE) {
			if (selMachine.getSelected() == false) {
				timeUsed += selMachine.getTimeUsed();
				gstate = GeneralState::IDLE;
			}
			else {
				uint8_t sid = selMachine.getSelection();
				selection.insert(sid);
				enabled.erase(sid);
				timeUsed += selMachine.getTimeUsed();
				myassert(timeUsed < timeLimit, "MultiSelect machine internal error 1");
				uint64_t specialTimeLimit = min(THRESHOLD, timeLimit - timeUsed);
				selMachine.config(choices, enabled, specialTimeLimit);
				selMachine.restart(evt.timestamp);
				state = StateCode::IN_NTH_SEL;
			}
		}
		break;
	}
}

void MultiSelectMachine::getSelection(BlockSet & ret)
{
	copyBlockSet(selection, ret);
}

uint64_t MultiSelectMachine::getTimeUsed()
{
	return timeUsed;
}
