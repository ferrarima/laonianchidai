#pragma once

#include "machines.h"
#include "blink.h"


class GameResultMachine : public Machine {
private:
	const static int REPEAT = 5;
	bool shining;
	int counter;
	Color c;
	BlinkMachine blinkMachine;
public:
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
};
