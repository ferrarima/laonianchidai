#include "myassert.h"
#include "logging.h"

void myassert(bool cond)
{
	if (!cond) {
		logError("Assert error");
		delay(9999999);
	}
}

void myassert(bool cond, const char * msg)
{
	if (!cond) {
		logError(msg);
		delay(9999999);
	}
}
