#include "publish.h"
#include "actions.h"
#include "myassert.h"

void MainMachine::restart(uint64_t timestamp)
{
	//auto act1 = createSoundAction(Sound::IDLE_BROWSING);
	//addToActionQue(act1);
	gstate = GeneralState::RUNNING;
	state = StateCode::WTING;
}

void MainMachine::handle(const Event & evt)
{
	myassert(gstate == GeneralState::RUNNING, "MainMachine not running but receives events");
	switch (state) {
	case StateCode::WTING:
		if (evt.type == EventType::EVT_PLAYER && evt.arg == 1) {
			readyMachine.restart(evt.timestamp);
			state = StateCode::READY_PHASE;
		}
		break;
	case StateCode::READY_PHASE:
		readyMachine.handle(evt);
		if (readyMachine.getGeneralState() == GeneralState::IDLE) {
			if (readyMachine.isReady()) {
				stg1Machine.restart(evt.timestamp);
				state = StateCode::IN_STAGE_1;
			}
			else {
				auto act1 = createSoundAction(Sound::IDLE_BROWSING);
				addToActionQue(act1);
				state = StateCode::WTING;
			}
		}
		break;
	case StateCode::IN_STAGE_1:
		stg1Machine.handle(evt);
		if (stg1Machine.getGeneralState() == GeneralState::IDLE) {
			if (stg1Machine.passed()) {
				stg2Machine.restart(evt.timestamp);
				state = StateCode::IN_STAGE_2;
				//resMachine.restart(evt.timestamp);
				//state = StateCode::IN_RESULT;
			}
			else {
				resMachine.restart(evt.timestamp);
				state = StateCode::IN_RESULT;
			}
		}
		break;
	case StateCode::IN_STAGE_2:
		stg2Machine.handle(evt);
		if (stg2Machine.getGeneralState() == GeneralState::IDLE) {
			resMachine.restart(evt.timestamp);
			state = StateCode::IN_RESULT;
			//if (stg2Machine.passed()) {
			//	stg3Machine.restart(evt.timestamp);
			//	state = StateCode::IN_STAGE_3;
			//}
			//else {
			//	resMachine.restart(evt.timestamp);
			//	state = StateCode::IN_RESULT;
			//}
		}
		break;
	case StateCode::IN_STAGE_3:
		stg3Machine.handle(evt);
		if (stg3Machine.getGeneralState() == GeneralState::IDLE) {
			if (stg3Machine.passed()) {
				resMachine.restart(evt.timestamp);
				state = StateCode::IN_RESULT;
			}
			else {
				resMachine.restart(evt.timestamp);
				state = StateCode::IN_RESULT;
			}
		}
		break;
	case StateCode::IN_RESULT:
		resMachine.handle(evt);
		if (resMachine.getGeneralState() == GeneralState::IDLE) {
			state = StateCode::WTING;
		}
		break;
	}
	logDebug("machine main: ");
	logDebug((int)state);
	logDebug("");
}
