#include "platform.h"
#include "memory_challenge_1.h"
#include "myassert.h"
#include "utils.h"


void MemoryChallengeLv1Machine::restart(uint64_t timestamp) {
    hintBlock = random(0, NUM_BLOCK);
    hintColor = randomNonOffColor();
    touchAnswer = random(0, NUM_BLOCK);

	ColorSet cset;getNonOffColorSet(cset);
	cset.erase(hintColor);

	for (int i = 0;i<NUM_BLOCK;++i) {
        if (i == touchAnswer) {
            choices.colors[i] = hintColor;
        }
        else {
			myassert(!cset.empty(), "MC1 needs more colors to start.");
			Color sel;
			cset.randomPick(sel);
			cset.erase(sel);
            choices.colors[i] = sel;
        }
    }
	
    gapMachine.setDuration(PRE_GAP_DURATION);
    gapMachine.restart(timestamp);
    gstate = GeneralState::RUNNING;
    state = StateCode::IN_PRE_GAP;
}

void MemoryChallengeLv1Machine::handle(const Event &evt) {
	myassert(gstate==GeneralState::RUNNING, "MC1 handle called while not running.");
	switch (state) {
	case StateCode::IN_PRE_GAP:
		gapMachine.handle(evt);
		if (gapMachine.getGeneralState() == GeneralState::IDLE) {
			hintMachine.config(hintBlock, hintColor, HINT_DURATION);
			hintMachine.restart(evt.timestamp);
			state = StateCode::HINT_STARTED;
		}
		break;
	case StateCode::HINT_STARTED:
		hintMachine.handle(evt);
		if (hintMachine.getGeneralState() == GeneralState::IDLE) {
			gapMachine.setDuration(IN_GAP_DURATION);
			gapMachine.restart(evt.timestamp);
			state = StateCode::IN_IN_GAP;
		}
		break;
	case StateCode::IN_IN_GAP:
		gapMachine.handle(evt);
		if (gapMachine.getGeneralState() == GeneralState::IDLE) {
			BlockSet bs; bs.fillAll();
			selMachine.config(choices, bs, ANS_TIME_LIMIT);
			selMachine.restart(evt.timestamp);
			state = StateCode::WTING_ANSWER;
		}
		break;
	case StateCode::WTING_ANSWER:
		selMachine.handle(evt);
		if (selMachine.getGeneralState() == GeneralState::IDLE) {
			if (selMachine.getSelected() && selMachine.getSelection() == touchAnswer) {
				correct = true;
				wowMachine.restart(evt.timestamp);
				state = StateCode::IN_WOW;
			}
			else {
				correct = false;
				oopsMachine.restart(evt.timestamp);
				state = StateCode::IN_OOPS;
			}
		}
		break;
	case StateCode::IN_WOW:
		wowMachine.handle(evt);
		if (wowMachine.getGeneralState() == GeneralState::IDLE) {
			gapMachine.setDuration(SUF_GAP_DURATION);
			gapMachine.restart(evt.timestamp);
			state = StateCode::IN_SUF_GAP;
		}
		break;
	case StateCode::IN_OOPS:
		oopsMachine.handle(evt);
		if (oopsMachine.getGeneralState() == GeneralState::IDLE) {
			gapMachine.setDuration(SUF_GAP_DURATION);
			gapMachine.restart(evt.timestamp);
			state = StateCode::IN_SUF_GAP;
		}
		break;
	case StateCode::IN_SUF_GAP:
		gapMachine.handle(evt);
		if (gapMachine.getGeneralState() == GeneralState::IDLE) {
			gstate = GeneralState::IDLE;
		}
		break;
	}
}

bool MemoryChallengeLv1Machine::succeeded()
{
	return  correct;
}
