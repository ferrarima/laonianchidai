#pragma once



#include "machines.h"
#include "mem_body_challenge_2.h"

class Stage2Machine : public Machine {
private:
	static const int TOTAL = 3;
	int counter;
	int score;
	MemBodyChallengeLv2 chaMachine;
public:
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
	bool passed();
};