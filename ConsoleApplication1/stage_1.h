#pragma once

#include "machines.h"
#include "memory_challenge_2.h"pu

class Stage1Machine : public Machine {
private:
	static const int TOTAL = 3;
	int counter;
	MemoryChallengeLv2 chaManage;
	int score;
public:
	virtual void restart(uint64_t timestamp) override;
	virtual void handle(const Event & evt) override;
	bool passed();
};

