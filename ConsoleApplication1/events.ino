#include "timers.h"
#include "platform.h"
#include "events.h"
#include "myassert.h"
#include "config.h"

const char* et2str(EventType et) {
	switch (et) {
	case EventType::EVT_PLAYER: return "PlayerEvent";
	case EventType::EVT_TIMER: return "TimerEvent";
	case EventType::EVT_TOUCH: return "TouchEvent";
	default: return "UnknownEvent";
	}
}


Event::Event(uint64_t timestamp, EventType type, uint8_t arg) {
    Event::type = type;
    Event::arg = arg;
    Event::timestamp = timestamp;
}

void Event::log(LogLevel lv) {
	switch (lv) {
	case LogLevel::DEBUG:
		logDebug("");
		logInfo("==============================");
		logDebug(timestamp);
		logDebug(et2str(type));
		logDebug(arg);
		logInfo("==============================");
		logError("");
		break;
	case LogLevel::INFO:
		logInfo("");
		logInfo("==============================");
		logInfo(timestamp);
		logInfo(et2str(type));
		logInfo(arg);
		logInfo("==============================");
		logError("");
		break;
	case LogLevel::WARNING:
		logWarning("");
		logInfo("==============================");
		logWarning(timestamp);
		logWarning(et2str(type));
		logWarning(arg);
		logInfo("==============================");
		logError("");
		break;
	case LogLevel::ERROR:
		logError("");
		logInfo("==============================");
		logError(timestamp);
		logError(et2str(type));
		logError(arg);
		logInfo("==============================");
		logError("");
		break;
	}
}


static TimerSet tset;
static bool sset[NUM_BLOCK];
static bool playerStatePre;
static bool playerStateCrt;
int itst0;//0: timers, 1: touches, 2: players
int itst1;
bool forceInvalid;

void refreshEventService(IO &io) {
	refreshTimers(tset);
	
	for (int i = 0; i < NUM_BLOCK; ++i)
		sset[i] = io.touchHappend(i);
	
	playerStatePre = playerStateCrt;
	playerStateCrt = io.hasPlayer();

	itst0 = 0;
	itst1 = 0;
	forceInvalid = false;
}


bool isvalid() {
	if (forceInvalid) return false;
	if (itst0 == 0)
		return tset.has(itst1);
	else if (itst0 == 1)
		return sset[itst1];
	else
		return (playerStateCrt != playerStatePre);
}

bool onestep() {
	if (itst0 == 2)
		return false;
	else if (itst0 == 1) {
		if (itst1 == NUM_BLOCK - 1) {
			itst0 = 2, itst1 = 0;
		}
		else {
			++itst1;
		}
	}
	else {
		if (itst1 == NUM_TIMERS - 1) {
			itst0 = 1, itst1 = 0;
		}
		else {
			++itst1;
		}
	}
	forceInvalid = false;
	return true;
}

bool hasNextEvent() {
	while (!isvalid()) {
		if (!onestep()) return false;
	}
	return true;
}

Event nextEvent() {
	while (!isvalid()) {
		myassert(onestep());
	}
	forceInvalid=true; 

	EventType type;
	uint8_t	arg;
	if (itst0 == 0) {
		type = EventType::EVT_TIMER;
		arg = itst1;
	}
	else if (itst0 == 1) {
		type = EventType::EVT_TOUCH;
		arg = itst1;
	}
	else {
		type = EventType::EVT_PLAYER;
		arg = (playerStateCrt) ? 1 : 0;
	}
	return Event(getCurrentTime(), type, arg);
}
