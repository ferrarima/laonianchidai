#include "io.h"
#include "platform.h"
#include "logging.h"

void VibIO::init()
{
	weightPin = A0;

	//for (int i = 0; i < SOUND_PIN_NUM; ++i) {
	//	soundPin[i] = 14 + i;
	//	pinMode(soundPin[i], OUTPUT);
	//}
	//for (int i = 0; i < SOUND_PIN_NUM; ++i)
	//	digitalWrite(soundPin[i], 1);

	touchPin[0] = 40;
	touchPin[1] = 41;
	touchPin[2] = 42; 
	touchPin[3] = 43;
	touchPin[4] = 44;
	touchPin[5] = 45;
	touchPin[6] = 46;
	touchPin[7] = 47;

	lightPin[0][0] = 22;
	lightPin[0][1] = 2;
	lightPin[0][2] = 23;

	lightPin[1][0] = 24;
	lightPin[1][1] = 3;
	lightPin[1][2] = 25;

	lightPin[2][0] = 26;
	lightPin[2][1] = 4;
	lightPin[2][2] = 27;

	lightPin[3][0] = 28;
	lightPin[3][1] = 5;
	lightPin[3][2] = 29;

	lightPin[4][0] = 30;
	lightPin[4][1] = 6;
	lightPin[4][2] = 31;

	lightPin[5][0] = 32;
	lightPin[5][1] = 7;
	lightPin[5][2] = 33;

	lightPin[6][0] = 34;
	lightPin[6][1] = 8;
	lightPin[6][2] = 35;

	lightPin[7][0] = 36;
	lightPin[7][1] = 9;
	lightPin[7][2] = 37;

	for (int i = 0; i < NUM_BLOCK; ++i) {
		pinMode(touchPin[i], INPUT);
	}
	for (int i = 0; i < NUM_BLOCK; ++i) {
		for (int j = 0; j < 3; ++j) {
			pinMode(lightPin[i][j], OUTPUT);
		}
	}

}


void VibIO::playSound(Sound sound)
{
	//int n = (int)sound;
	//digitalWrite(soundPin[1], !(n & 1));
	//digitalWrite(soundPin[2], !((n & 2) >> 1));
	//digitalWrite(soundPin[3], !((n & 4) >> 2));
	//digitalWrite(soundPin[4], !((n & 8) >> 3));
	//digitalWrite(soundPin[5], !((n & 16) >> 4));

}

void VibIO::mute()
{
}

void VibIO::setLight(uint8_t blockId, Color color)
{
	int vr, vg, vb;
	getColorDigitalValue(color,vr,vg,vb);
	analogWrite(lightPin[blockId][0], vr);
	analogWrite(lightPin[blockId][1], vg);
	analogWrite(lightPin[blockId][2], vb);
}

void VibIO::sample()
{
	for (int i = 0; i < NUM_BLOCK; ++i) {
		touchAcc[i] = (touchCrt[i] == 0) ? 0 : min(100000000, touchAcc[i] + 1);
		touchCrt[i] = digitalRead(touchPin[i]);
	}
//	logDebug(touchCrt[0]);
	kgCrt = analogRead(weightPin);
}

bool VibIO::touchHappend(uint8_t i)
{
	if (i >= NUM_BLOCK) return false;
	return touchCrt[i] == 0 && touchAcc[i]>100;
}

bool VibIO::hasPlayer()
{
	return kgCrt > 20;
}
