#pragma once
#include <stdint.h>

const int NUM_TIMERS=10;

enum class TimerState {
    IDLE,
    RUNNING,
};

struct TimerSet {
	bool v[NUM_TIMERS];
	bool has(uint8_t timerId);
};


TimerState getTimerState(uint8_t tid);
bool startTimer(uint8_t tid, uint64_t span);
void forceStartTimer(uint8_t tid, uint64_t span);
void StopTimer(uint8_t tid);
void refreshTimers(TimerSet &ret);
uint64_t getCurrentTime();