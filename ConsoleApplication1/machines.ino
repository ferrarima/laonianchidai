#include "config.h"
#include "platform.h"
#include "logging.h"
#include "events.h"
#include "sounds.h"
#include "myassert.h"
#include "machines.h"



GeneralState Machine::getGeneralState()
{
    return gstate;
}

void Machine::terminate()
{
	gstate = GeneralState::IDLE;
}


void WowMachine::restart(uint64_t timestamp) {

	auto act1=createForceStartTimerAction(TIMER_ID, WOW_DURATION);
	addToActionQue(act1);
    
	auto act2=createSoundAction(Sound::WOW);
	addToActionQue(act2);
    
	gstate = GeneralState::RUNNING;
}

void WowMachine::handle(const Event &evt) {
	myassert(gstate==GeneralState::RUNNING, "WowMachine received events when not running");
    if (evt.type == EventType::EVT_TIMER&&evt.arg == TIMER_ID) {
        gstate = GeneralState::IDLE;
    }
}


void OopsMachine::restart(uint64_t timestamp) {
    
	auto act1=createForceStartTimerAction(TIMER_ID, OOPS_DURATION);
	addToActionQue(act1);
    
	auto act2=createSoundAction(Sound::OOPS);
	addToActionQue(act2);
    
	gstate = GeneralState::RUNNING;
}

void OopsMachine::handle(const Event &evt) {
	myassert(gstate == GeneralState::RUNNING, "OopsMachine received events when not running");
    if (evt.type == EventType::EVT_TIMER&&evt.arg == TIMER_ID) {
        gstate = GeneralState::IDLE;
    }
}


void SilentGapMachine::setDuration(uint64_t duration) {
    SilentGapMachine::duration = duration;
}

void SilentGapMachine::restart(uint64_t timestamp) {
    auto act1=createForceStartTimerAction(TIMER_ID, duration);
	addToActionQue(act1);

	ColorPattern cp; cp.setAllToOFF();
    auto act2=createAllLightsAction(cp);
	addToActionQue(act2);
	
	auto act3=createMuteAction();
	addToActionQue(act3);
	
	gstate = GeneralState::RUNNING;
}

void SilentGapMachine::handle(const Event &evt) {
	myassert(gstate == GeneralState::RUNNING, "SilentGapMachine received events when not running");
    if (evt.type == EventType::EVT_TIMER&&evt.arg == TIMER_ID) {
        gstate = GeneralState::IDLE;
    }
}


void SingleHintMachine::config(uint8_t blockId, Color color, uint64_t duration){
    hintBlock = blockId;
    hintColor = color;
    SingleHintMachine::duration = duration;
}

void SingleHintMachine::restart(uint64_t timestamp){
    auto act1 = createForceStartTimerAction(TIMER_ID, duration);
	addToActionQue(act1);
	
	ColorPattern cp; cp.setAllToOFF();
    auto act2=createAllLightsAction(cp);
	addToActionQue(act2);
	
	auto act3=createOneLightAction(hintBlock, hintColor);
	addToActionQue(act3);
	
	gstate=GeneralState::RUNNING;
}

void SingleHintMachine::handle(const Event &evt){
	myassert(gstate == GeneralState::RUNNING, "SingleHintMachine received events when not running");
    if (evt.type == EventType::EVT_TIMER&&evt.arg == TIMER_ID) {
        gstate = GeneralState::IDLE;
    }
}


void SingleSelectMachine::config(const ColorPattern &cp, const BlockSet &enabled, uint64_t timeLimit) {
	copyBlockSet(enabled, SingleSelectMachine::enabled);
	copyColorPattern(cp, choices);
	SingleSelectMachine::timeLimit = timeLimit;
}

void SingleSelectMachine::restart(uint64_t timestamp){
	selected = false;
	timeUsed = 0;

    startTime = timestamp;

	auto act1 = createForceStartTimerAction(TIMER_ID, timeLimit);
	addToActionQue(act1);
	
	auto act2=createAllLightsAction(choices);
	addToActionQue(act2);

	gstate = GeneralState::RUNNING;
}

void SingleSelectMachine::handle(const Event &evt){
	myassert(gstate == GeneralState::RUNNING, "SingleSelect machine received events when not running");
    if (evt.type==EventType::EVT_TIMER && evt.arg==TIMER_ID) {
        timeUsed=timeLimit;
        selected=false;
        gstate=GeneralState::IDLE;
    }
	else if (evt.type==EventType::EVT_TOUCH && enabled.has(evt.arg)) {
        timeUsed=evt.timestamp-startTime;
        selected=true;
        selection=evt.arg;
        gstate=GeneralState::IDLE;
    }
}

bool SingleSelectMachine::getSelected(){
    return selected;
}

uint8_t SingleSelectMachine::getSelection(){
    return selection;
}

uint64_t SingleSelectMachine::getTimeUsed(){
    return timeUsed;
}



void MultiHintMachine::config(const ColorPattern & hint, uint64_t duration)
{
	copyColorPattern(hint, MultiHintMachine::hint);
	MultiHintMachine::duration = duration;
}

void MultiHintMachine::restart(uint64_t timestamp)
{
	auto act1 = createForceStartTimerAction(TIMER_ID, duration);
	addToActionQue(act1);

	auto act2 = createAllLightsAction(hint);
	addToActionQue(act2);

	gstate = GeneralState::RUNNING;
}

void MultiHintMachine::handle(const Event & evt)
{
	myassert(gstate==GeneralState::RUNNING, "MultiHint machine received events when not running.");
	if (evt.type == EventType::EVT_TIMER && evt.arg == TIMER_ID) {
		gstate = GeneralState::IDLE;
	}
}